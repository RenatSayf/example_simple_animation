package renatsayf.example_simple_animation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

/*
1.  В папку ресурсов res добавить новую директорию anim.
2.  В папку anim добавить файлы анимации с кодом необходимой анимации

 */

public class MainActivity extends AppCompatActivity
{
    //3. константы для ID пунктов конткстного меню TextView
    final int MENU_ALPHA_ID = 1;
    final int MENU_SCALE_ID = 2;
    final int MENU_TRANSLATE_ID = 3;
    final int MENU_ROTATE_ID = 4;
    final int MENU_COMBO_ID = 5;

    TextView textView1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //4. Элемент к которому будет применятся анимация
        textView1 = (TextView) findViewById(R.id.text_view1);

        //5. Регистрируем контекстное меню для компонента textView2
        registerForContextMenu(textView1);
    }

    //6. Создание контекстного меню для TextView
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        // добавляем пункты
        if (v == textView1)
        {
            menu.add(0, MENU_ALPHA_ID, 0, "alpha");
            menu.add(0, MENU_SCALE_ID, 0, "scale");
            menu.add(0, MENU_TRANSLATE_ID, 0, "translate");
            menu.add(0, MENU_ROTATE_ID, 0, "rotate");
            menu.add(0, MENU_COMBO_ID, 0, "combo");
        }
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    Animation anim;
    Animation anim_trans_from_top;


    //7. Обработчик выбора элемента в контекстном меню
    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        anim = null;
        anim_trans_from_top = null;
        // определяем какой пункт был нажат
        switch (item.getItemId())
        {
            case MENU_ALPHA_ID:
                // создаем объекты анимации из файлов anim/...
                anim = AnimationUtils.loadAnimation(this, R.anim.myalpha);
                break;
            case MENU_SCALE_ID:
                anim = AnimationUtils.loadAnimation(this, R.anim.myscale);
                break;
            case MENU_TRANSLATE_ID:
                anim = AnimationUtils.loadAnimation(this, R.anim.mytrans);
                anim_trans_from_top = anim;
                break;
            case MENU_ROTATE_ID:
                anim = AnimationUtils.loadAnimation(this, R.anim.myrotate);
                break;
            case MENU_COMBO_ID:
                anim = AnimationUtils.loadAnimation(this, R.anim.mycombo);
                break;
        }
        //8. Запускаем анимацию для компонента textView2
        textView1.startAnimation(anim);

        //9. Устанавливаем слушатель начала, окончания и повтора анимации
        anim.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {

            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                if (animation.equals(anim_trans_from_top))
                {
                    anim_trans_from_top = AnimationUtils.loadAnimation(MainActivity.this, R.anim.anim_trans_from_top);
                    textView1.startAnimation(anim_trans_from_top);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {

            }
        });
        return super.onContextItemSelected(item);
    }
}
